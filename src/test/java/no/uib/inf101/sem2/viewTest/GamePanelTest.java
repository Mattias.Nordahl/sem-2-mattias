package no.uib.inf101.sem2.viewTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.swing.JFrame;

import no.uib.inf101.sem2.controller.KeyboardInput;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.model.Food;
import no.uib.inf101.sem2.model.Model;
import no.uib.inf101.sem2.model.Snake;
import no.uib.inf101.sem2.view.GamePanel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for the GamePanel class.
 */
public class GamePanelTest {
    private GamePanel gamePanel;
    private Model model;
    private KeyboardInput keyboardInput;

    /**
     * Initializes the necessary objects before each test.
     */
    @BeforeEach
    public void setUp() {
        model = new Model();
        gamePanel = new GamePanel(model, keyboardInput);
    }

    /**
     * Test the incrementScore method in the GamePanel class.
     */
    @Test
    public void testIncrementScore() {
        // Increment the score and check if it was updated correctly
        model.incrementScore();
        assertEquals(1, model.getScore(), "The score should be incremented by 1");
    }

    /**
     * Test the rendering/displaying of the GamePanel.
     * This test only checks if the GamePanel can be rendered without errors.
     * It does not check the correctness of the rendered content.
     */
    @Test
    public void testRendering() {
        JFrame testFrame = new JFrame();
        testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        testFrame.setContentPane(gamePanel);
        testFrame.pack();
        testFrame.setVisible(true);

        // Wait for a short period to visually check the rendering
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        testFrame.dispose();
    }
}
