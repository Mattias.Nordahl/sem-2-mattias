package no.uib.inf101.sem2.modelTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.Snake;

import static org.junit.jupiter.api.Assertions.*;

public class SnakeTest {
    private Snake snake;

    @BeforeEach
    public void setUp() {
        snake = new Snake(10, 10);
    }

    @Test
    public void testGrow() {
        int initialSize = snake.getBody().size();
        snake.grow();
        assertTrue(snake.isGrow());
        snake.getBody().addFirst(new GridCell(11, 10));
        assertEquals(initialSize + 1, snake.getBody().size());
    }

    @Test
    public void testSetDirection() {
        snake.setDirection(1, 0);
        assertEquals(1, snake.getDx());
        assertEquals(0, snake.getDy());
    }

    @Test
    public void testIsCellInSnake() {
        GridCell cellInSnake = new GridCell(10, 10);
        GridCell cellNotInSnake = new GridCell(5, 5);
        assertTrue(snake.isCellInSnake(cellInSnake));
        assertFalse(snake.isCellInSnake(cellNotInSnake));
    }

    @Test
    public void testReset() {
        snake.setDirection(1, 0);
        snake.grow();
        snake.reset(5, 5);
        assertEquals(1, snake.getBody().size());
        assertEquals(5, snake.getBody().getFirst().getX());
        assertEquals(5, snake.getBody().getFirst().getY());
    }
}