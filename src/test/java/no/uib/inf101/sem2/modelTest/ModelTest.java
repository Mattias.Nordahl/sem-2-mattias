package no.uib.inf101.sem2.modelTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.Model;

/**
 * Test class for Model. It contains tests for the moveSnake() function and
 * all aspects of the checkCollisions() function.
 */
public class ModelTest {
    private Model model;

    /**
     * Set up the initial conditions for the test scenarios.
     */
    @BeforeEach
    public void setup() {
        this.model = new Model();
        
    }

    /**
     * Test the moveSnake() function to ensure the snake moves correctly.
     */
    @Test
    public void testMoveSnake() {
        model.setSnakeDirection(1, 0);
        model.moveSnake();
        assertEquals(11, model.getSnake().getBody().getFirst().getX());
        assertEquals(10, model.getSnake().getBody().getFirst().getY());
    }

    @Test
    public void testRespawnFood() {
        model.respawnFood();
        assertEquals(GridCell.Value.FOOD, model.getGrid().getCell(model.getFoodLocation().getX(), model.getFoodLocation().getY()).getValue());
    }

    /**
     * Test the checkCollisions() function to ensure the snake detects boundary collisions.
     */
    @Test
    public void testSnakeCollidesWithEdges() {
        model.getSnake().getBody().set(0, new GridCell(0, 10));
        model.setSnakeDirection(-1, 0);
        model.moveSnake();
        assertTrue(model.checkCollisions());
    }

    /**
     * Test the checkCollisions() function to ensure the snake detects collisions with itself.
     */
    @Test
    public void testSnakeCollidedWithItself() {
        // Grow the snake to a length of 8
        model.growSnake();
        model.moveSnake();
        model.growSnake();
        model.moveSnake();
        model.growSnake();
        model.moveSnake();
        model.growSnake();
        model.moveSnake();
        model.growSnake();
        model.moveSnake();
        model.growSnake();
        model.moveSnake();

        //Turn the snake so it collides with itself
        model.setSnakeDirection(1, 0);
        model.moveSnake();
        model.setSnakeDirection(0, 1);
        model.moveSnake();
        model.setSnakeDirection(-1, 0);
        model.moveSnake();
        model.setSnakeDirection(0, -1);
        model.moveSnake();
        assertTrue(model.checkCollisions());
    }

    

 

    @Test
    public void testIncrementScore() {
        model.incrementScore();
        assertEquals(1, model.getScore());
    }

    @Test
    public void testReset() {
        model.setSnakeDirection(1, 0);
        model.moveSnake();
        model.reset();
        assertEquals(10, model.getSnake().getBody().getFirst().getX());
        assertEquals(10, model.getSnake().getBody().getFirst().getY());
        assertEquals(0, model.getScore());
    }
    
    
}
