package no.uib.inf101.sem2.modelTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.Food;

import static org.junit.jupiter.api.Assertions.*;

public class FoodTest {
    private Food food;

    @BeforeEach
    public void setUp() {
        food = new Food();
    }

    @Test
    public void testSetLocation() {
        GridCell location = new GridCell(5, 5);
        food.setLocation(location);
        assertEquals(location, food.getLocation());
    }

    @Test
    public void testGetLocation() {
        GridCell location = new GridCell(7, 3);
        food.setLocation(location);
        GridCell retrievedLocation = food.getLocation();
        assertEquals(location, retrievedLocation);
    }

    @Test
    public void testReset() {
        GridCell location = new GridCell(9, 9);
        food.setLocation(location);
        food.reset();
        assertNull(food.getLocation());
    }
}
