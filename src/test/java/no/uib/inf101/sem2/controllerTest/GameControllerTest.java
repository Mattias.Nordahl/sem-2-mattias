package no.uib.inf101.sem2.controllerTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.controller.GameController;
import no.uib.inf101.sem2.controller.KeyboardInput;
import no.uib.inf101.sem2.model.Model;
import no.uib.inf101.sem2.view.GamePanel;
import no.uib.inf101.sem2.view.GameWindow;

/**
 * Test class for GameController. It contains tests for the creation of the
 * GameController object and the game window visibility.
 */
public class GameControllerTest {
    private GameController gameController;
    private GamePanel gamePanel;
    private Model model;
    private KeyboardInput keyboardInput;
    private GameWindow gameWindow;

    /**
     * Set up the initial conditions for the test scenarios.
     */
    @BeforeEach
    public void setup() {
        model = new Model();
        gameWindow = new GameWindow(model);
        gameWindow.setVisible(true);
        gamePanel = new GamePanel(model, keyboardInput);
        gameController = new GameController(model, gameWindow);
    }

    /**
     * Test the creation of a GameController object.
     */
    @Test
    public void testGameControllerCreation() {
        assertNotNull(gameController, "GameController should not be null");
    }

    /**
     * Test the visibility of the game window in GameController.
     */
    @Test
    public void testGameControllerWindowVisibility() {
        GameWindow gameWindow = gameController.getGameWindow();
        assertNotNull(gameWindow, "GameWindow should not be null");
        assertTrue(gameWindow.isVisible(), "GameWindow should be visible");
    }
    
}
