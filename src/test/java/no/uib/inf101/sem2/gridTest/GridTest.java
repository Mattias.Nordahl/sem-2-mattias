package no.uib.inf101.sem2.gridTest;

import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for the Grid class.
 */

public class GridTest {
    private Grid grid;

    /**
     * Create a new grid with the given width and height.
     */
    @BeforeEach
    public void setup() {
        grid = new Grid(20, 20);
    }

    /**
     * Tests that the width of the grid is equal to the width given in the constructor.
     */
    @Test
    public void testGetWidth() {
        assertEquals(20, grid.getWidth());
    }

    /**
     * Tests that the height of the grid is equal to the height given in the constructor.
     */
    @Test
    public void testGetHeight() {
        assertEquals(20, grid.getHeight());
    }

    /**
     * Tests that the getCell function returns something else than null when given a valid position.
     * Tests that the getCell function returns null when given an invalid position.
     */
    @Test
    public void testGetCell() {
        assertNotEquals(null, grid.getCell(15, 15));
        assertNotEquals(null, grid.getCell(0, 0));
        assertEquals(null, grid.getCell(-5, -5));
    }

    /**
     * Tests that the isCellEmpty function returns true when given a position with an empty cell.
     * And that it returns false when given a position with a non-empty cell.
     */
    @Test
    public void testIsCellEmpty() {
        GridCell cell = grid.getCell(5, 5);
        assertEquals(true, grid.isCellEmpty(cell));
        grid.setCellValue(5, 5, GridCell.Value.SNAKE);
        assertEquals(false, grid.isCellEmpty(cell));
    }

    /**
     * Tests that the setCellValue function sets the value of the cell at the given position 
     * to the given value.
     */
    @Test
    public void testSetCellValue() {
        assertNotEquals(GridCell.Value.FOOD, grid.getCell(5,5).getValue());
        grid.setCellValue(5, 5, GridCell.Value.FOOD);
        assertEquals(GridCell.Value.FOOD, grid.getCell(5,5).getValue());
    }
    
}
