package no.uib.inf101.sem2.gridTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.GridCell;

public class GridCellTest {
    private GridCell cell;

    /**
     * Create a new grid cell with the given coordinates.
     */
    @BeforeEach
    public void setup() {
        cell = new GridCell(5, 10);
    }

    /**
     * Tests that the x coordinate of the cell is equal to the x coordinate given in the constructor.
     */
    @Test
    public void testGetX() {
        assertEquals(5, cell.getX());
    }

    /**
     * Tests that the y coordinate of the cell is equal to the y coordinate given in the constructor.
     */
    @Test
    public void testGetY() {
        assertEquals(10, cell.getY());
    }

    /**
     * Tests that the getValue function returns the correct value.
     * Tests that the setValue function sets the correct value.
     */
    @Test
    public void testGetValueAndSetValue() {
        assertEquals(GridCell.Value.EMPTY, cell.getValue());

        cell.setValue(GridCell.Value.FOOD);
        assertEquals(GridCell.Value.FOOD, cell.getValue());

        cell.setValue(GridCell.Value.SNAKE);
        assertEquals(GridCell.Value.SNAKE, cell.getValue());
    }

    /**
     * Tests that the isEmpty function returns the correct value.
     */
    @Test
    public void testIsEmpty() {
        assertEquals(true, cell.isEmpty());

        cell.setValue(GridCell.Value.FOOD);
        assertEquals(false, cell.isEmpty());

        cell.setValue(GridCell.Value.SNAKE);
        assertEquals(false, cell.isEmpty());
    }





    
}
