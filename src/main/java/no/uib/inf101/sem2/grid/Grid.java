package no.uib.inf101.sem2.grid;

/**
 * The Grid class represents a two-dimensional grid of GridCell objects. It implements the IGrid interface.
 */

public class Grid implements IGrid {
    public GridCell[][] cells;
    private int width;
    private int height;
    public static final int CELL_SIZE = 20; // The size of each cell in pixels


    /**
     * Create a new Grid with the given width and height.
     * @param width
     * @param height
     */
    public Grid (int width, int height) {
        this.width = width;
        this.height = height;
        cells = new GridCell[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                cells[i][j] = new GridCell(i, j);
            }
        }
    }

    @Override
    public GridCell getCell(int x, int y) {
        if(x >= 0 && x < width && y >= 0 && y < height) {
            return cells[x][y];
        }
        return null;
    }

    @Override
    public boolean isCellEmpty(GridCell cell) {
        if(cell != null) {
            return cell.isEmpty();
        }
        return false;
    }

    @Override
    public void setCellValue(int x, int y, GridCell.Value value) {
        GridCell cell = getCell(x, y);
        if(cell != null) {
            cell.setValue(value);
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }
    
}
