package no.uib.inf101.sem2.grid;

public interface IGrid {


    /**
     * Get the cell at the given position.
     * @param x the x coordinate
     * @param y the y coordinate
     * @return the cell at the given position
     */
    GridCell getCell(int x, int y);

    /**
     * Check if the cell at the given position is empty.
     * @param x the x coordinate
     * @param y the y coordinate
     * @return true if the cell is empty, false otherwise
     */
    boolean isCellEmpty(GridCell cell);

    /**
     * Set the value of the cell at the given position.
     * @param x the x coordinate
     * @param y the y coordinate
     * @param value the new value of the cell
     */
    void setCellValue(int x, int y, GridCell.Value value);

    /**
     * Get the width of the grid.
     * @return the width of the grid
     */
    int getWidth();

    /**
     * Get the height of the grid.
     * @return the height of the grid
     */
    int getHeight();
   
}
