package no.uib.inf101.sem2.grid;


/**
 * Represents a single cell in a grid.
 */

public class GridCell {
    private int x;
    private int y;
    private Value value;


    /**
     * Create a new GridCell with the given coordinates and set the value to EMPTY.
     * @param x the x coordinate
     * @param y the y coordinate
     * @param value the value of the cell
     */
    public GridCell(int x, int y) {
        this.x = x;
        this.y = y;
        this.value = Value.EMPTY;
    }

    /**
     * Enum for the possible values of a cell.
     */
    public enum Value {
        EMPTY, SNAKE, FOOD
    }

    /**
     * Get the x coordinate of the cell.
     * @return the x coordinate
     */
    public int getX() {
        return x;
    }


    /**
     * Get the y coordinate of the cell.
     * @return the y coordinate
     */
    public int getY() {
        return y;
    }

    /**
     * Get the value of the cell.
     * @return the value of the cell
     */
    public Value getValue() {
        return value;
    }

    /**
     * Set the value of the cell.
     * @param value the new value of the cell
     */
    public void setValue(Value value) {
        this.value = value;
    }


    /**
     * Check if the cell is empty.
     * @return true if the cell is empty, false otherwise
     */
    public boolean isEmpty() {
        return value == Value.EMPTY;


    }

    /**
     * Override the equals method to compare two GridCells.
     * Checks if two cells have the same y and x coordinates, and the same value
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GridCell other = (GridCell) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        if (this.value != other.value) {
            return false;
        }
        return true;
    }


    
}
