package no.uib.inf101.sem2.controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;


import no.uib.inf101.sem2.model.Model;
import no.uib.inf101.sem2.view.GameWindow;



/**
 * The GameController class is responsible for managing the game loop and handling user input.
 * It updates the game state, checks for collisions, and repaints the game window as needed.
 */
 public class GameController implements ActionListener {
    private Timer timer;
    private Model model;
    private GameWindow gameWindow;


    /**
     * Constructs a new GameController object with the given model and game window.
     * @param model the Model object representing the game state
     * @param gameWindow the GameWindow object representing the game window
     */
    public GameController(Model model, GameWindow gameWindow) {
        this.model = model;
        this.gameWindow = gameWindow;
        this.timer = new Timer(100, GameController.this);
        KeyboardInput keyboardInput = new KeyboardInput(model);
        gameWindow.setListeners(
                new StartGameListener(),
                new ShowInstructionsListener(),
                new ExitGameListener(),
                new ReturnToMainMenuListener(),
                new CloseInstructionsListener(),
                keyboardInput
        );
    }

    /**
     * Updates the game state, checks for collisions, and repaints the game window.
     * @param e the ActionEvent object
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        model.moveSnake();
        if (model.checkCollisions()) {
            timer.stop();
            gameWindow.setGameOverPanel();
        }
        gameWindow.repaint();

    }

    /**
     * The StartGameListener class starts the game when an action is performed.
     */
    public class StartGameListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            
            gameWindow.setGamePanel();
            timer.start();
        }
    }

    /**
     * The ReturnToMainMenuListener class returns to the main menu when an action is performed.
     */
    public class ReturnToMainMenuListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.reset();
            gameWindow.setStartScreen();
        }
    }

    /**
     * The ShowInstructionsListener class shows the instructions when an action is performed.
     */
    public class ShowInstructionsListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            gameWindow.setInstructionsPanel();
        }
    }

    
    /**
     * The CloseInstructionsListener class closes the instructions when an action is performed.
     */
    public class CloseInstructionsListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            gameWindow.setStartScreen();
        }
    }


    /**
     * The ExitGameListener class exits the game when an action is performed.
     */
    public static class ExitGameListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }


    /**
     * Returns the GameWindow instance. Used for testing.
     * @return the GameWindow instance
     */
    public GameWindow getGameWindow() {
        return gameWindow;
    }



}





