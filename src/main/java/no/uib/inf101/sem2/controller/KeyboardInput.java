package no.uib.inf101.sem2.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import no.uib.inf101.sem2.model.Model;


/**
 * This class listens for keyboard input and updates the snake's direction accordingly.
 */

 public class KeyboardInput implements KeyListener {
    private Model model;

    /**
     * Constructor for KeyboardInput class
     * @param model the model that controls the snake that the KeyboardInput will control
     */
    public KeyboardInput(Model model) {
        this.model = model;
        
    }

    /**
     * Sets the direction of the snake based on which arrow key is pressed.
     * @param e the KeyEvent object that contains information about the key that was pressed.
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();

        switch (keyCode) {
            case KeyEvent.VK_UP:
                if (model.getSnakeDY() == 0) {
                    model.setSnakeDirection(0, -1);
                }
                break;
            case KeyEvent.VK_DOWN:
                if (model.getSnakeDY() == 0) {
                    model.setSnakeDirection(0, 1);
                }
                break;
            case KeyEvent.VK_LEFT:
                if (model.getSnakeDX() == 0) {
                    model.setSnakeDirection(-1, 0);
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (model.getSnakeDX() == 0) {
                    model.setSnakeDirection(1, 0);
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // Do nothing
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Do nothing
    }
}


