package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.GridCell;

/**
 * The Food class represents a food item in the snake game.
 * It has a location on the game grid and can be reset.
 */
public class Food implements IFood  {
    private GridCell location;

    /**
     * Constructor for the Food class.
     */
    public Food() { 
    }

    @Override
    public GridCell getLocation() {
        return location;
    }

    @Override
    public void setLocation(GridCell location) {
        this.location = location;
    }

    /**
     * Resets the food location to null.
     */
    @Override
    public void reset() {
        setLocation(null);
    }
    
    
    
    
}
