package no.uib.inf101.sem2.model;

import java.util.Random;


import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;


/**
 * The Model class represents the game state, including the snake, grid, food, and score.
 * It is responsible for handling game logic, such as moving the snake, checking collisions,
 * and updating the game state.
 */
public class Model {
    private Snake snake;
    private Grid grid;
    private Food food;
    private int score;
    private Random random = new Random();


    /**
     * Constructs a new Model object, initializing the snake, grid, food, and score.
     */
    public Model() {
        this.snake = new Snake(10, 10);
        this.grid = new Grid(20, 20);
        this.food = new Food();
        this.random = new Random();
        this.score = 0;
        
        // Initialize the snake's starting position in the grid
        GridCell snakeHead = snake.getBody().getFirst();
        grid.setCellValue(snakeHead.getX(), snakeHead.getY(), GridCell.Value.SNAKE);

        // Initialize the food's starting position in the grid
        respawnFood();
    }

    /**
     * Moves the snake according to its current direction and updates the grid accordingly.
     * This method also checks for collisions with food and grows the snake if necessary.
     */
    public void moveSnake() {
        int newX = snake.getBody().getFirst().getX() + snake.getDx();
        int newY = snake.getBody().getFirst().getY() + snake.getDy();
    
        // Check if the new position is within the grid boundaries
        if (newX >= 0 && newX < grid.getWidth() && newY >= 0 && newY < grid.getHeight()) {
            GridCell newCell = grid.getCell(newX, newY);
    
            if (newCell.getValue() == GridCell.Value.FOOD) {
                snake.grow();
                respawnFood();
                incrementScore();
            }
        }
    
        if (!snake.isGrow()) {
            grid.setCellValue(snake.getBody().getLast().getX(), snake.getBody().getLast().getY(), GridCell.Value.EMPTY);
            snake.getBody().removeLast();
        } else {
            snake.setGrow(false);
        }
    
        snake.getBody().addFirst(new GridCell(newX, newY));
        grid.setCellValue(newX, newY, GridCell.Value.SNAKE);
    
        // Reset the direction flag after updating the direction
        snake.setDirectionChanged(false);
    }
    
    

    /**
     * Spawns a new food item in a random empty cell on the grid.
     */
    public void respawnFood() {
        while (true) {
            int x = random.nextInt(grid.getWidth());
            int y = random.nextInt(grid.getHeight());
            GridCell newFood = new GridCell(x, y);

            if (grid.isCellEmpty(newFood) && !snake.isCellInSnake(newFood)) {
                if (food.getLocation() != null) {
                    grid.setCellValue(food.getLocation().getX(), food.getLocation().getY(), GridCell.Value.EMPTY); // Clear the old food location
                }
                food.setLocation(newFood);
                grid.setCellValue(newFood.getX(), newFood.getY(), GridCell.Value.FOOD);
                break;
            }
        }
    }

    /**
     * Checks if the snake collided with itself or the edges of the game grid.
     * If a collision is detected, the game over screen is displayed.
     */
    public boolean checkCollisions() {
        // Check if snake collided with itself or the edges
        GridCell head = snake.getBody().getFirst();
        return snakeCollidedWithItself() || snakeCollidedWithEdges(head);
      }
    

    /**
     * Checks if the snake collided with itself
     * @return true if the snake collided with itself, false otherwise
     */
    private boolean snakeCollidedWithItself() { 
        for (int i = 1; i < snake.getBody().size(); i++) {
            if (snake.getBody().getFirst().equals(snake.getBody().get(i))) {
                return true;
            }
        }
        return false;
    }

        /**
     * Checks if the snake collided with the edges of the game grid
     * @param head the head of the snake
     * @return true if the snake collided with the edges of the game grid, false otherwise
     */

     private boolean snakeCollidedWithEdges(GridCell head) {
        int x = (int) head.getX();
        int y = (int) head.getY();
        return x < 0 || x >= grid.getWidth() || y < 0 || y >= grid.getHeight();
    }

    /**
     * Increments the player's score by 1.
     */
    public void incrementScore() {
        score++;
    }

    /**
     * Returns the current score.
     * @return the current score
     */
    public int getScore() {
        return score;
    }

    /**
     * Returns the current grid.
     * @return the current grid
     */
    public Grid getGrid() {
        return grid;
    }


    /**
     * Resets the game state to the initial state.
     * @param newGrid
     */
    public void reset() {
        grid = new Grid(20, 20);
        snake.reset(10, 10);
        snake.setDirection(0, 0);
        food.reset();
        respawnFood();
        resetScore();
    }

    /**
     * Returns the current snake.
     * @return
     */
    public Snake getSnake() {
        return snake;
    }

    /**
     * Resets the score to 0.
     */
    public void resetScore() {
        this.score = 0;
    }

    /**
     * retrieves the snake's change in x direction
     * @return
     */
    public int getSnakeDX() {
        return snake.getDx();
    }

    /**
     * retrieves the snake's change in y direction
     * @return
     */
    public int getSnakeDY() {
        return snake.getDy();
    }

    /**
     * sets the snake's direction
     * @param dx
     * @param dy
     */
    public void setSnakeDirection(int dx, int dy) {
        snake.setDirection(dx, dy);
    }

    /**
     * retrieves the food's location
     * Used for testing purposes
     * @return the food's location
     */
    public GridCell getFoodLocation() {
        return food.getLocation();
    }

    /**
     * sets the food's location
     * Used for testing purposes
     * @param x
     * @param y
     */
    public void setFoodLocation(int x, int y) {
        food.setLocation(new GridCell(x, y));
    }

    /**
     * sets the snake's location
     * Used for testing purposes
     * @param x
     * @param y
     */
    public void growSnake() {
        snake.grow();
    }
    
}
    
    
    





    

