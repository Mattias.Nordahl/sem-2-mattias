package no.uib.inf101.sem2.model;


import java.util.LinkedList;

import no.uib.inf101.sem2.grid.GridCell;

/**
 * Interface for the Snake class.
 * The reason for creating an interface here is to have the possibility to
 * potentially crate different snakes for a multiplayer
 */

public interface ISnake {

    /**
     * Grow the snake by one unit.
     */
    void grow();

    /**
     * Set the direction of the snake.
     * 
     * @param dx The change in x position.
     * @param dy The change in y position.
     */
    void setDirection(int dx, int dy);

    /**
     * Get the body of the snake.
     * 
     * @return The body of the snake.
     */
    LinkedList<GridCell> getBody();

    /**
     * Get the change in x position.
     * @return the change in x position
     */
    int getDx();

    /**
     * Get the change in y position.
     * @return the change in y position
     */
    int getDy();

    /**
     * Check if the cell is part of the snake.
     * Used so the food doesn't spawn on the snake.
     * @param cell the cell to check
     * @return true if the cell is part of the snake, false otherwise
     */
    boolean isCellInSnake(GridCell cell); 
    
}
