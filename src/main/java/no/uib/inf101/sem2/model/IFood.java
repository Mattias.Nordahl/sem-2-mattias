package no.uib.inf101.sem2.model;


import no.uib.inf101.sem2.grid.GridCell;

/**
 * Interface for the Food class.
 * The reason for creating an interface here is to have the possibility to
 * potentially (if time) create different types of food on the grid (bombs, powerups, etc.)
 */
public interface IFood {

    /**
     * Get the location of the food.
     * 
     * @return The location of the food.
     */
    GridCell getLocation();


    /**
     * Set the location of the food.
     * 
     * @param location The location of the food.
     */
    void setLocation(GridCell location);

    /**
     * Reset the food.
     */
    void reset();
    
}
