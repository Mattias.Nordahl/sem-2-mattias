package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.GridCell;
import java.util.LinkedList;

/**
 * The Snake class represents the snake in the game.
 * It contains a list of GridCells representing the body of the snake and related methods for managing the snake.
 */
public class Snake implements ISnake {
    private LinkedList<GridCell> body;
    private boolean grow;
    private int dx;
    private int dy;
    private boolean directionChanged; 

    /**
     * Constructor for the Snake class.
     * Initializes the snake with a starting position (x, y).
     * @param x The x-coordinate of the snake's starting position.
     * @param y The y-coordinate of the snake's starting position.
     */
    public Snake(int x, int y) {
        body = new LinkedList<GridCell>();
        body.add(new GridCell(x, y));
        grow = false;
        dx = 0;
        dy = 0;
        directionChanged = false; 
    }

    @Override
    public void grow() {
        grow = true;
    }

    @Override
    public void setDirection(int dx, int dy) {
        //Check if the direction has already been changed
        if(directionChanged) {  
            return; 
        }
        
        //Check if the new direction is opposite of the current direction
        if(this.dx == -dx && this.dy == -dy) { 
            return;
        }
        
        this.dx = dx;
        this.dy = dy;

        //Set the direction flag to true after updating the direction
        directionChanged = true; 
    }

    @Override
    public LinkedList<GridCell> getBody() {
        return body;
    }

    @Override
    public int getDx() {
        return dx;
    }

    @Override
    public int getDy() {
        return dy;
    } 

    @Override
    public boolean isCellInSnake(GridCell cell) {
        //Iterate through the snake's body
        for (GridCell bodyCell : body) {
            //Check if the cell is part of the snake
            if (bodyCell.getX() == cell.getX() && bodyCell.getY() == cell.getY()) {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if the snake is set to grow.
     * @return true if the snake is set to grow,
     *  * false otherwise.
    */
    public boolean isGrow() {
        return grow;
    }

    
    /**
     * Sets whether the snake should grow or not.
     * @param grow true if the snake should grow, false otherwise.
     */
    public void setGrow(boolean grow) {
        this.grow = grow;
    }

    /**
     * Sets whether the snake's direction has changed or not.
     * @param directionChanged true if the direction has changed, false otherwise.
     */
    public void setDirectionChanged(boolean directionChanged) {
        this.directionChanged = directionChanged;
    }

    /**
     * Resets the snake to its initial state with a new starting position (startX, startY).
     * @param startX The x-coordinate of the snake's new starting position.
     * @param startY The y-coordinate of the snake's new starting position.
     */
    public void reset(int startX, int startY) {
        body.clear();
        body.add(new GridCell(startX, startY));
    }
     
}
