package no.uib.inf101.sem2;

import no.uib.inf101.sem2.controller.GameController;
import no.uib.inf101.sem2.model.Model;
import no.uib.inf101.sem2.view.GameWindow;

/**
 * The Main class is the entry point for the Snake game application.
 */
public class Main {

    public static void main(String[] args) {
        Model model = new Model();
        GameWindow gameWindow = new GameWindow(model);
        new GameController(model, gameWindow);

        gameWindow.setStartScreen();
    }
}


