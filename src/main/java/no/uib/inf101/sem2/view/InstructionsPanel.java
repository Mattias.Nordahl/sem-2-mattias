package no.uib.inf101.sem2.view;


import no.uib.inf101.sem2.grid.Grid;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


/**
 * The InstructionsPanel class is responsible for displaying the game instructions
 * on a JPanel. It contains a JTextArea to show the instructions, a return button,
 * and a background image.
 */
public class InstructionsPanel extends JPanel {

    private final Image backgroundImage = new ImageIcon("background.jpg").getImage();

    /**
     * Constructs a new InstructionsPanel with the given ActionListener for the return button.
     * Initializes the layout, JTextArea, and return button.
     * @param closeInstructionsListener the ActionListener for the return button
     */
    public InstructionsPanel(ActionListener closeInstructionsListener) {
        setLayout(new BorderLayout());
        setFocusable(true);

        JTextArea instructionsText = new JTextArea();
        instructionsText.setText("Instructions:\n\n" +
                "1. Use the arrow keys to control the snake.\n" +
                "2. Eat the food to grow and increase your score.\n" +
                "3. Avoid colliding with the edges or yourself.\n");
        instructionsText.setEditable(false);
        instructionsText.setFont(new Font("Arial", Font.PLAIN, 14));
        add(instructionsText, BorderLayout.CENTER);

        JButton returnButton = new JButton("Return to main menu"); // Create a return button
        returnButton.addActionListener(closeInstructionsListener); // Add the ActionListener to the return button
        add(returnButton, BorderLayout.SOUTH); // Add the return button to the bottom of the panel
        setPreferredSize(new Dimension(Grid.CELL_SIZE * 20, Grid.CELL_SIZE * 20));
    }

    /**
     * Overrides the paintComponent method to paint the background image.
     * @param g the Graphics object used for painting
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), null);
    }
}
