package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
* The GameOverPanel class is a custom JPanel that displays a "Game Over" message
* and a button to return to the main menu when the game is over.
*/
public class GameOverPanel extends JPanel {
    private JLabel gameOverLabel;
    private JButton returnButton;

    /**
    * Constructs a new GameOverPanel with the provided ActionListener for the return button.
    * @param returnToMainMenuListener the ActionListener to handle the "Return to main menu" button click
    */
    public GameOverPanel(ActionListener returnToMainMenuListener) {
        setLayout(new GridBagLayout()); // Set the layout manager to GridBagLayout
        setBackground(new Color(255, 0, 0, 150));
        GridBagConstraints constraints = new GridBagConstraints(); // Create GridBagConstraints for component placement

        gameOverLabel = new JLabel("Game Over");
        gameOverLabel.setForeground(Color.WHITE);
        gameOverLabel.setFont(gameOverLabel.getFont().deriveFont(50f));
        constraints.gridy = 0; // Set the vertical grid position for the label
        add(gameOverLabel, constraints);

        returnButton = new JButton("Return to main menu");
        returnButton.addActionListener(returnToMainMenuListener);
        constraints.gridy = 1; // Set the vertical grid position for the button
        add(returnButton, constraints);

    }
    
}
