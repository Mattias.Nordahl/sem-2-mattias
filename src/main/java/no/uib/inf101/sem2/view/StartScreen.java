package no.uib.inf101.sem2.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.Grid;

/**
 * The StartScreen class represents the starting screen of the game.
 * It contains buttons for starting the game, showing instructions, and exiting the game.
 */
public class StartScreen extends JPanel {

    /**
     * Constructs a new StartScreen with the given ActionListeners.
     * @param startGameListener         ActionListener for starting the game
     * @param showInstructionsListener  ActionListener for showing instructions
     * @param exitGameListener          ActionListener for exiting the game
     */
    public StartScreen(ActionListener startGameListener, ActionListener ShowInstructionsListener, ActionListener exitGameListener) {
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        // Create the Start Game button and add the ActionListener
        JButton startButton = new JButton("Start Game");
        startButton.addActionListener(startGameListener);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(0, 0, 10, 0); // Set padding between the buttons
        add(startButton, constraints);

        // Create the Instructions button and add the ActionListener
        JButton instructionsButton = new JButton("Instructions");
        instructionsButton.addActionListener(ShowInstructionsListener);
        constraints.gridy = 1;
        add(instructionsButton, constraints);

        // Create the Exit button and add the ActionListener
        JButton exitGameButton = new JButton("Exit");
        exitGameButton.addActionListener(exitGameListener);
        constraints.gridy = 2;
        add(exitGameButton, constraints);

        setFocusable(true);
        setPreferredSize(new Dimension(Grid.CELL_SIZE * 20, Grid.CELL_SIZE * 20));
        
    }
    
}

