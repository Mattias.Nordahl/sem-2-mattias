package no.uib.inf101.sem2.view;

import javax.swing.JPanel;


import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.Model;
import java.awt.Font;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyListener;

/**
 * The GamePanel class is responsible for rendering the game grid and the score
 * on the screen. It extends the JPanel class and overrides the paintComponent method
 * to paint the grid and the score.
 */

 public class GamePanel extends JPanel {
    private Model model;
    private int cellSize; //The size of each cell in the grid


    /**
     * Creates a new GamePanel with the given Snake, Food and Grid objects.
     * @param snake the Snake object
     * @param food the Food object
     * @param grid the Grid object
     */
    public GamePanel(Model model, KeyListener keyListener) {
        this.model = model;
        this.cellSize = 20;
        this.addKeyListener(keyListener);
        setPreferredSize(new Dimension(model.getGrid().getWidth() * cellSize, model.getGrid().getHeight() * cellSize));
        setBackground(Color.BLACK);

    }

    /**
     * Overrides the paintComponent method to paint the grid and the score on the screen.
     * @param g the Graphics object used for painting
     */

     @Override
     protected void paintComponent(Graphics g) {
         super.paintComponent(g);
     
         // Draw grid
         for (int y = 0; y < model.getGrid().getHeight(); y++) {
             for (int x = 0; x < model.getGrid().getWidth(); x++) {
                 GridCell cell = model.getGrid().getCell(x, y);
                 if (cell.getValue() == GridCell.Value.EMPTY) {
                     g.setColor(Color.BLACK);
                 } else if (cell.getValue() == GridCell.Value.SNAKE) {
                     g.setColor(Color.GREEN);
                 } else if (cell.getValue() == GridCell.Value.FOOD) {
                     g.setColor(Color.RED);
                 }
                 g.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
                 g.setColor(Color.BLACK);
                 g.drawRect(x * cellSize, y * cellSize, cellSize, cellSize);
             }
         }

        // Draw score
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 14));
        g.drawString("Score: " + model.getScore(), 10, 15);
     }
    



}

