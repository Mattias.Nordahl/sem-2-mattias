package no.uib.inf101.sem2.view;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;

import no.uib.inf101.sem2.controller.GameController;
import no.uib.inf101.sem2.controller.KeyboardInput;
import no.uib.inf101.sem2.model.Model;

/**
 * The GameWindow class represents the main window for the game. 
 * It manages the different panels (start screen, game panel, game over panel, instructions panel) 
 * and switches between them as necessary.
 */
public class GameWindow extends JFrame {
    private StartScreen startScreen;
    private GamePanel gamePanel;
    private GameOverPanel gameOverPanel;
    private InstructionsPanel instructionsPanel;
    private final Model model;
  
    /**
     * Constructs a new GameWindow object with the given model.
     * @param model the Model object representing the game state
     */
    public GameWindow(Model model) {
      this.model = model;
    }
  
    /**
     * Sets the listeners for the different panels.
     * @param startGameListener the listener for starting the game
     * @param showInstructionsListener the listener for showing the instructions
     * @param exitGameListener the listener for exiting the game
     * @param returnToMainMenuListener the listener for returning to the main menu
     * @param closeInstructionsListener the listener for closing the instructions
     * @param keyboardInput the listener for keyboard input
     */
    public void setListeners(
      GameController.StartGameListener startGameListener,
      GameController.ShowInstructionsListener showInstructionsListener,
      GameController.ExitGameListener exitGameListener,
      GameController.ReturnToMainMenuListener returnToMainMenuListener,
      GameController.CloseInstructionsListener closeInstructionsListener,
      KeyboardInput keyboardInput
    ) {
      this.startScreen = new StartScreen(startGameListener, showInstructionsListener, exitGameListener);
      this.gameOverPanel = new GameOverPanel(returnToMainMenuListener);
      this.instructionsPanel = new InstructionsPanel(closeInstructionsListener);
      this.gamePanel = new GamePanel(model, keyboardInput);
      this.gamePanel.add(gameOverPanel);
    }
  
    /**
     * Sets the given panel as the active panel in the window.
     * @param panel the panel to be set as active
     */
    private void setPanel(JPanel panel) {
      getContentPane().removeAll();
      getContentPane().add(panel);
      getContentPane().setPreferredSize(new Dimension(panel.getPreferredSize()));
      pack();
      setResizable(false);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setLocationRelativeTo(null);
      setVisible(true);
  
      panel.requestFocusInWindow();
    }
  
    /**
     * Sets the start screen as the active panel.
     */
    public void setStartScreen() {
      setPanel(startScreen);
    }
  
    /**
     * Sets the game panel as the active panel.
     */
    public void setGamePanel() {
      gameOverPanel.setVisible(false);
      setPanel(gamePanel);
    }
  
    /**
     * Sets the game over panel as the active panel.
     */
    public void setGameOverPanel() {
      gameOverPanel.setVisible(true);
    }
  
    /**
     * Sets the instructions panel as the active panel.
     */
    public void setInstructionsPanel() {
      setPanel(instructionsPanel);
    }
  }



 
