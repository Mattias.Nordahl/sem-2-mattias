package no.uib.inf101.sem2.view;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;

/**
 * The ScorePanel class is responsible for displaying the player's score on a JPanel.
 * It contains a JLabel to display the score and updates it when the score is incremented.
 */
public class ScorePanel extends JPanel {
    private int score;
    private JLabel scoreLabel;

    /**
     * Constructs a new ScorePanel, initializes the score to 0,
     * sets the layout, and adds the score label to the panel.
     */
    public ScorePanel() {
        score = 0;
        setLayout(new GridLayout(1, 1));
        scoreLabel = new JLabel("Score: " + score);
        scoreLabel.setFont(new Font("Arial", Font.PLAIN, 16));
        add(scoreLabel);
    }

    /**
     * Increments the player's score by 1 and updates the score label.
     */
    public void incrementScore() {
        score++;
        scoreLabel.setText("Score: " + score);
    }
}