# Snake

**Forklaring av programmet:** <br>
Dette programmet et et enkelt Snake-spill der spilleren kontrollerer en slange som beveger seg rundt på et rutenett (grid). Hovedmålet er å navigere slangen for å spise maten som vises på rutenettet samtidig som man unngår kollisjoner med veggene til rutenettet og slangens egen kropp. <br>

Når spillet starter er slangen liten, men vokser lengre for hver gang den spiser mat. Etter hvert som slangen vokser, blir spillet mer utfordrende da det er større sjanse for å kollidere med slangens egen kropp. Spillerens poengsum øker med 1 for hver mat som spises, og spillet avsluttes når slangen enten kolliderer med en vegg eller med seg selv. <br>

Spillet har en startskjerm der spilleren kan velge å starte spillet, se instruksjoner eller avslutte spillet. Det er også en game over-skjerm som vises når spillet avsluttes, slik at spilleren kan gå tilbake til hovedmenyen.

For å spille spillet bruker spilleren piltastene på tastaturet til kontrollere hvilken retning slangen beveger seg i. Spillskjermen viser poengsum, slangen, mat og rutenettet, og oppdaterer disse elementene i sanntid ettert som spillet spilles. 


**Link til video:** <br>
https://www.youtube.com/watch?v=1GOW1DrYAy4


